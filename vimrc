" Personal vimrc file of Hugo Martín <hugomartin89@gmail.com>
"
" While much of it is beneficial for general use, I would recommend
" picking out the parts you want and understand.
"
" Don't forget to read the LICENSE.txt file :)
"
" CONTENT:
"   1. General settings
"       1.1. Basic
"       1.2. Interface
"       1.3. Editor
"   2. Plug-ins
"       2.1 General
"       2.2 Language/Syntax support
"       2.3 Color schemes
"   3. Functions
"   4. Autocommands
"   5. Commands
"   6. Maps
"   7. Color scheme
"   8. Specific settings

" ==============================================================================
" 1. General settings

" ------------------------------------------------------------------------------
" 1.1 Basic

    set nocompatible
    set encoding =utf-8

    set fileencoding  =utf-8
    set fileencodings =utf-8,ucs-bom,cp1251
    set fileformat    =unix
    set fileformats   =unix,dos,mac

    set mouse    =a
    set ttymouse =sgr

    set backupdir =~/.vim/tmp
    set directory =~/.vim/tmp

" ------------------------------------------------------------------------------
" 1.2 Interface

    set noshowmode
    set wildmenu
    set laststatus=2

" ------------------------------------------------------------------------------
" 1.3 Editor

    syntax on

    set nowrap

    set list
    set listchars =tab:>\ ,trail:~

    set relativenumber
    set number

    set cursorline

    set expandtab
    set tabstop     =4
    set shiftwidth  =4
    set softtabstop =4
    set colorcolumn =81

    set foldmethod   =indent
    set foldnestmax  =10
    set foldlevel    =1
    set nofoldenable

" ==============================================================================
" 2. Plug-ins

" TODO: add support for hasefroch
let g:vim_dir     = '~/.vim'
let g:plugins_dir = vim_dir . '/bundle'
let g:vundle_dir  = plugins_dir . '/Vundle.vim'

function InstallVundle()
    silent execute '!mkdir -p ' . g:plugins_dir
    if !isdirectory(expand(g:plugins_dir))
        throw 'Error creating plugins dir'
    endif

    silent execute '!git clone -q https://github.com/VundleVim/Vundle.vim ' . g:vundle_dir
    if !isdirectory(expand(g:vundle_dir))
        throw 'Error trying to clone Vundle repository'
    endif
endfunction

" check if Vundle is installed
if isdirectory(expand(g:vundle_dir))
    " TODO: check if plugins are not installed

    let g:vundle_is_installed = 1
else
    " nope? ok, I try to install it for you

    if executable('git')
        echomsg 'Installing Vundler...'

        try
            call InstallVundle()

            let g:vundle_is_installed  = 1
            let g:need_install_plugins = 1
        catch /Error.*/
            " :ohgodwhy:

            echomsg 'Ups! Something went wrong :('
        endtry

    else
        " ups, it's impossible without Git

        echomsg 'Git is needed for Vundle'
    endif
endif

" so..
" if Vundle is already installed I need initialize it, right?
" if not :yaoming:
if exists('g:vundle_is_installed')
    filetype off

    execute 'set runtimepath+=' . g:vundle_dir
    call vundle#rc()

    unlet g:vim_dir
    unlet g:plugins_dir
    unlet g:vundle_dir

" ------------------------------------------------------------------------------
" 2.1 General

    Bundle 'VundleVim/Vundle.vim'

    Bundle 'editorconfig-vim'
    Bundle 'tpope/vim-fugitive'
    Bundle 'airblade/vim-gitgutter'
    Bundle 'buddhavs/vim-tasklist'
    Bundle 'edsono/vim-matchit'
    Bundle 'godlygeek/tabular'
    Bundle 'mattn/emmet-vim'
    Bundle 'mtth/scratch.vim'
    Bundle 'tomtom/tcomment_vim'
    Bundle 'vim-airline/vim-airline-themes'

    Bundle 'scrooloose/nerdtree'
        let NERDTreeIgnore    = ['.pyc$[[file]]']
        let g:NERDTreeWinSize = 35

    Bundle 'majutsushi/tagbar'
        let g:tagbar_width = 30

    Bundle 'vim-airline/vim-airline'
        let g:airline_symbols        = {}
        let g:airline_left_sep       = ''
        let g:airline_right_sep      = ''
        let g:airline_symbols.linenr = '¶'
        let g:airline_symbols.branch = '⎇'
        let g:airline_symbols.paste  = 'ρ'

    Bundle 'edkolev/tmuxline.vim'
        let g:tmuxline_powerline_separators = 0
        let g:tmuxline_preset = {
            \'a'    : '#S',
            \'b'    : '#P | #W',
            \'c'    : '',
            \'win'  : '#I | #W',
            \'cwin' : '#I | #W',
            \'x'    : '',
            \'y'    : '%d-%b-%y | %H:%M',
            \'z'    : '#H'
        \}

    Bundle 'ctrlpvim/ctrlp.vim'
        let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$|node_modules'

    Bundle 'Raimondi/delimitMate'
        let delimitMate_expand_cr = 1

" ------------------------------------------------------------------------------
" 2.2 Language/Syntax support

    Bundle 'leafgarland/typescript-vim'
    Bundle 'kchmck/vim-coffee-script'
    Bundle 'groenewege/vim-less'
    Bundle 'digitaltoad/vim-jade'

" ------------------------------------------------------------------------------
" 2.3 Color schemes

    Bundle 'morhetz/gruvbox'
        let g:gruvbox_contrast_light = 'hard'
        let g:gruvbox_contrast_dark  = 'hard'

" ------------------------------------------------------------------------------
    " maybe I need to install plugins
    if exists('g:need_install_plugins') && g:need_install_plugins
        " yep, plugins are not installed yet... go ahead

        echomsg 'Installing plugins...'
        BundleInstall
        quit

        unlet g:need_install_plugins
    endif

    filetype plugin indent on

    unlet g:vundle_is_installed
endif

" ==============================================================================
" 3. Functions

" delete spaces and tabs at end of line
function! DeleteTrailingWhiteSpaces()
    execute "normal mz"
    %s/\s\+$//ge
    execute "normal `z"
endfunction

" toggle relative numbers
function! ToggleRelativeNumbers()
    if &relativenumber == 1
        set norelativenumber
        set number
    else
        set relativenumber
        set number
    endif
endfunction

" checks if a color scheme exists
function! HasColorScheme(name)
    let a:__path = 'colors/' . a:name . '.vim'
    return !empty(globpath(&runtimepath, a:__path))
endfunction

" ==============================================================================
" 4. Autocommands

    " auto delete trailing white spaces on save
    autocmd BufWrite * :call DeleteTrailingWhiteSpaces()

    " for a best navigation experience
    autocmd InsertEnter * :call ToggleRelativeNumbers()
    autocmd InsertLeave * :call ToggleRelativeNumbers()

    " specific filetype
    autocmd FileType c      :set filetype=c.doxygen
    autocmd FileType c++    :set filetype=c++.doxygen
    autocmd FileType pascal :setlocal tabstop=2 shiftwidth=2 softtabstop=2
    autocmd FileType jade   :setlocal tabstop=2 shiftwidth=2 softtabstop=2
    autocmd FileType html   :setlocal tabstop=2 shiftwidth=2 softtabstop=2
    autocmd FileType css    :setlocal tabstop=2 shiftwidth=2 softtabstop=2
    autocmd FileType sass   :setlocal tabstop=2 shiftwidth=2 softtabstop=2

" ==============================================================================
" 5. Commands

command! DarkTheme  set background=dark
command! LightTheme set background=light

" ==============================================================================
" 6. Maps

execute "set <M-h>=\eh"
execute "set <M-l>=\el"

" indent in visual mode
vmap <TAB> >
vmap <S-TAB> <

" a more comfortable way to leave to normal mode
map <C-c> <ESC>

nmap <silent> <M-h> :tabprevious<CR>
nmap <silent> <M-l> :tabnext<CR>

nmap <silent> <C-h> :wincmd h<CR>
nmap <silent> <C-j> :wincmd j<CR>
nmap <silent> <C-k> :wincmd k<CR>
nmap <silent> <C-l> :wincmd l<CR>

nmap <silent> \| :NERDTreeToggle<CR>

nmap <silent> <F9> :TagbarToggle<CR>

" ==============================================================================
" 7. Color scheme

if &t_Co >= 256
    if HasColorScheme('gruvbox')
        colorscheme gruvbox

        set background=dark
    endif
elseif &t_Co >= 8
    colorscheme elflord
endif

" ==============================================================================
" 8. Speciffic settings

if &term =~ '256color'
    " disable Background Color Erase (BCE) so that color schemes
    " render properly when inside 256-color tmux and GNU screen.
    " see also http://snk.tuxfamily.org/log/vim-256color-bce.html
    set t_ut=
endif
